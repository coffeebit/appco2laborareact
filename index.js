/*
 * Module dependencies
 */

var express = require('express');
var http = require('http');

const port = 3001;
var app = express();

// Configurar la ruta de archivos estáticos
app.use('/', express.static(__dirname + '/public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.get('/signIn', (req, res) => {
  res.sendFile(__dirname + '/signIn.html');
});

app.get('/dashboard', (req, res) => {
  res.sendFile(__dirname + '/dashboard.html');
});

app.get('/tips', (req, res) => {
  res.sendFile(__dirname + '/dashboard.html');
});

app.get('/info', (req, res) => {
  res.sendFile(__dirname + '/info.html');
});

var server = http.createServer(app).listen(port, () => {
  console.log(`El servidor está escuchando en el puerto ${port}`);
});


