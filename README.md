# README #

### ¿Qué es Colabora? ###

* Es una App Web que apoya el concepto verde y el cuidado de nuestro planeta. Su principal función es informar a las personas acerca de cómo influye la presencia del Dióxido de Carbono en nuestra atmósfera, así como informar las medidas necesarias para ayudar a nuestro planeta y al mismo tiempo beneficiar a nuestra economía. Con la ayuda de un asistente personal dentro de la aplicación, recordatorios y tips seguramente podremos crear una relación Ganar - Ganar, ayudamos a nuestro planeta y ahorramos dinero.
* Prototipo V1.0
* http://co2labora.coffeebit.us/